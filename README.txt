Main Purpose:
The main goal of this project is to draw three different versions of the
Sierpinski Gasket using HTML canvas, JavaScript, and WebGL. All three versions
must have different dimensional characteristics. The purpose for doing this is to
get a better understanding of how the gasket’s characteristics affect the final
output.

Instructions:
Run SierpGaskV1.html and SierpGaskV2.html to view the slightly different 2d
Sierpinski Gaskets. Run SierpGaskV3.html to view the 3d Sierpinski Gasket.